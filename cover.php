<header class="business-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-12  text-center">
                <br><br><br><br><br>
                <hr>
                <h1 class="blog-title"><a href="<?php echo get_bloginfo( 'wpurl' );?>"><?php echo get_bloginfo( 'name' ); ?></a></h1>
                <hr>
                <p class="blog-description lead"><?php echo get_bloginfo( 'description' ); ?></p>
                <br>
                <a class="btn btn-success btn-md" href="#">Load</a>
            </div>
        </div>
    </div>
</header>