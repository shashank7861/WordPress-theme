<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo get_bloginfo( 'name' ); ?></title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo get_bloginfo('template_directory'); ?>/style.css" rel="stylesheet">
    <link href="<?php echo get_bloginfo('template_directory'); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_bloginfo('template_directory'); ?>/assets/css/nav.css" rel="stylesheet">
    <?php wp_head(); ?>
  </head>
  <body bgcolor="red">
  	<nav class="navbar navbar-default">
        <div class="container">
	        <div class="row" >
	            <!-- Brand and toggle get grouped for better mobile display -->
	            <div class="navbar-header page-scroll">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	            </div>
	            <a class="navbar-brand" href="<?php echo get_bloginfo( 'wpurl' );?>"><h3 style="font-weight:600;"><?php echo get_bloginfo( 'name' ); ?></h3></a>
	            <!-- Collect the nav links, forms, and other content for toggling -->
	            <div class="collapse navbar-collapse" id="navbar-collapse">
	                <ul class="nav navbar-nav">
						<li><a href="<?php echo get_bloginfo( 'wpurl' );?>">Home</a></li>
                            <?php wp_list_pages( '&title_li=' ); ?>
	                </ul>
	            </div><!-- /.navbar-collapse -->
	        </div><!-- /.container-fluid -->
        </div>
    </nav>
    <div id="particles"></div>
    <div class="container">